# README for a flexible aDNA-pipeline from fastq to bamfile with Snakemake

-- this pipeline is still under construction --  please contact ilektra.schulz@unifr.ch for questions, suggestions, bugs, etc....

>                           ___
>                             {-)   |\
>                        [m,].-"-.   /
>      [][__][__]          \(/\__/\)/
>      [__][__][__][__]~~~~   |  |
>      [][__][__][__][__][]  /   |
>      [__][__][__][__][__] | /| |
>      [][__][__][__][__][] | || |  ~ ~ ~  
>      [__][__][__][__][__] __,__,  \__/  
> ejm


This is a pipeline, used to analyze bamfiles deriving from ancient DNA.

## BEFORE RUNNING PIPELINE

- this pipeline is constructed using the workflow management system tool [Snakemake](https://snakemake.readthedocs.io/en/stable/)

- to ensure package-continuity, a [conda-environment](https://conda.io/docs/user-guide/tasks/manage-environments.html) is provided alongside. You can build an environment using the file 'environment.yaml' from within this package, or ensure that all listed programms and packages are installed on your system.

- cluster compatibility for SLURM cluster systems is included.

# Sample names
Your files need to be named in the following manner:

MyFastqFile_R1_001.fastq.gz

The placeholder "MyFastqFile" is absolutely not crucial! It can contain any combination of signs and letters (except regex) and does not need to be informative on your library or sample.
It has to be unique throughout your samples though.

The second part (\_R1_001.fastq.gz) is crucial and needs to be named exactly like this (for paired-end of course with R2 for reverse file).

This means, that older hiseq-runs that were split into several files ending on *R1_001.fastq.gz, *R1_002.fastq.gz, a.s.o. need to be concatenated prior to running the pipeline, as they are also not meeting illuminas naming-convention standard anymore (https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/NamingConvention_FASTQ-files-swBS.htm).

# Sample list
You will have to provide a tab-separated sample list with the following columns:

Sample    Lib    File    Path

*Sample*  
--> all samples with the same "sample" column will be merged. This column will be put to the bamfile header as "SM"

*Lib*  
--> all samples that come from the same PCR experiment get the same library-number.  
--> This column is put to LB in the header  
--> this means also, that two PCR-parallels (even if you could technically call them the same "library") have to get an individual tag here since all samples with the same "Lib" will undergo duplicate removal between them.  

*File*  
--> this is the first part from the example above and is the index for snakemake to identify the sample (has to be unique)  
--> will be the ID in the header  
--> only once for paired-end samples as well  

*Path*  
--> provide the absolute path to the folder containing your sample (including the last "/" for the folder)

# Config file
please change the .example.config.yaml to config.yaml to use the pipeline.
Within this config file, adapt the locations of your reference and program files, and also define how your samples have been sequenced (single-end or paired-end).
Further explenations can be found within the config-file.

# Conda
To ensure package continuity, please use the provided conda environment by running the following command:  
	conda env create --name Raw_Pipeline_Environment --file environment.yaml

Before starting a snakemake session call this environment by executing:  
	source activate Raw_Pipeline_Environment


# results
You will recieve several result-files:

a) your finished bamfile per sample --> analysis/9.merge/{sample}.bam

b) the FileStats --> analysis/x.FileStats/{file}*,  
Containing the flagstat and BamDiagnostics-results for each of the files prior to merging.

c) the COUNTS per library --> analysis/COUNTS,  
one table containing all the estimations per library before merging.

d) the sample-stats --> analysis/x.SamStats/{sample}*,  
Containing the flagstat and BamDiagnostics-results from the merged sample.

e) the DAG --> pipeline-${SLURM_JOB_ID}.pdf,  
a dag showing the rule dependencies

