#import libraries
import pandas as pd
from pathlib import Path
import os
#import pdb; ipdb.set_trace() 

#define configfile
configfile: "config.yaml"

#load librarylist as dataframe
df = pd.read_csv(config['sample_file'], sep='\t', index_col='File', comment='#')

#define variables
SEQUENCE = config['sequence']
FILE = df.index
SAMPLE= df.Sample
LIB= df['Lib']
FQ_PATH = df["Path"]
ADAPTERS = config["Adapter"]

#print("file = %s" % FILE)
#print("sample = %s" % SAMPLE)
#print("lib = %s" % LIB)
############################################################################
##------------------define dependencies and pipeline-paths
############################################################################
#define which input file to use for alignment
def trimmed():
    if (SEQUENCE == "single"):
        return ['analysis/3.trimmed/{file}_R1_001_trimmed.fq.gz']
    else:
        return ['analysis/3.trimmed/{file}_R1_001_val_1.fq.gz','analysis/3.trimmed/{file}_R2_001_val_2.fq.gz']

#filter for mapped reads in single and paired end
def filterMap():
    if (SEQUENCE == "single"):
        return ['-F 4']
    else:
        return ['-f 3']

############################################################################
##------------------Start of Snakefile
############################################################################


#this is the rule snakemake calls by default.
rule all:
    input:
    	"analysis/FILE_COUNTS",
        "analysis/SAM_COUNTS",
        expand("analysis/summaries/{sample}.summary.txt", zip, sample=SAMPLE)#,
        #expand("analysis/summaries/{file}.summary.txt", zip, file=FILE)

include: "scripts/rawcount.snakefile"
include: "scripts/trim.snakefile"
include: "scripts/align.snakefile"
include: "scripts/merge.snakefile"
include: "scripts/counts.snakefile"


rule summary_file:
    input:
        merged="analysis/x.SamStats/{sample}_BamDiagnostics.log",
        BamD1=lambda wildcards: expand('analysis/y.FileCounts/{file}.counts', file = df.index[ df.Sample == wildcards.sample]),
        fastqc1_pre=lambda wildcards: expand('analysis/2.fastqc/{file}_R1_001_fastqc.html', file = df.index[ df.Sample == wildcards.sample])
    output:
        file="analysis/summaries/{sample}.summary.txt"
    shell:
        """
        echo -e '{input.fastqc1_pre} {input.BamD1} {input.merged}' > {output.file}
        """




