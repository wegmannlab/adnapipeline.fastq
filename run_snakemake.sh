#!/bin/sh
#SBATCH --err snakemake-%A.err
#SBATCH --out snakemake-%A.out
#SBATCH --ntasks-per-node=8
#SBATCH --mem=1G
#SBATCH --time=150:00:00
#SBATCH --job-name=Snakemake

rm analysis/summaries/*summary.txt

mkdir -p analysis/
mkdir -p analysis/logs

##### 	execute this script using sbatch run_snakemake.sh
#####	if a prior version of snakemake crashed, execute this script using sbatch run_snakemake.sh rerun


if [ -z "$1" ]; then
snakemake -p --latency-wait 360 -j 50 --cluster-config cluster.json --cluster "sbatch --mem={cluster.mem} --time {cluster.time} --output {cluster.out} --error {cluster.err} --job-name {cluster.name} --ntasks {cluster.ntasks}"

snakemake --dag --latency-wait 360 -j 50 --cluster-config cluster.json --cluster "sbatch" | dot -Tpdf > pipeline-${SLURM_JOB_ID}.pdf

else
snakemake --unlock --rerun-incomplete
snakemake -p --rerun-incomplete --latency-wait 360 -j 50 --cluster-config cluster.json --cluster "sbatch --mem={cluster.mem} --time {cluster.time} --output {cluster.out} --error {cluster.err} --job-name {cluster.name} --ntasks {cluster.ntasks}"

snakemake --dag --rerun-incomplete --latency-wait 360 -j 50 --cluster-config cluster.json --cluster "sbatch" | dot -Tpdf > pipeline-${SLURM_JOB_ID}.pdf

fi

