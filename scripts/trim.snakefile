if (SEQUENCE == "single"):
    rule trim:
        input:
            r1=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz")
        params:
            ad1=config["AdapterSequence1"],
            qual=config["qualityFilter"],
            leng=config["lengthFilter"]
        output:
            #fastqc1="analysis/2.fastqc/{file}_R1_001_fastqc.html",
            trimmed="analysis/3.trimmed/{file}_R1_001_trimmed.fq.gz",
            fastqc2="analysis/2.fastqc/{file}_R1_001_trimmed_fastqc.html",
            report="analysis/3.trimmed/{file}_R1_001.fastq.gz_trimming_report.txt"
        shell:
            """
            test=$(echo {params.ad1})
            echo "applying adapter sequence: ${{test}}"
            if [[ ${{test}} == "default" ]]; then adapter=" " ; else adapter=$(echo "-a ${{test}}"); fi
            #fastqc {input.r1} -o analysis/2.fastqc;
            trim_galore \
            -o analysis/3.trimmed/ \
            -q {params.qual} \
            --fastqc \
            --fastqc_args '--outdir analysis/2.fastqc/' \
            --length {params.leng}  \
            $adapter \
            {input.r1}
            """

else:
    rule trim:
        input:
            r1=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz"),
            r2=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R2_001.fastq.gz")
            #r1=infast()
        params:
            ad1=config["AdapterSequence1"],
            ad2=config["AdapterSequence2"],
            qual=config["qualityFilter"],
            leng=config["lengthFilter"]
        output:
            #fastqc1_pre="analysis/2.fastqc/{file}_R1_001_fastqc.html",
            trimmed1="analysis/3.trimmed/{file}_R1_001_val_1.fq.gz",
            fastqc1_post="analysis/2.fastqc/{file}_R1_001_val_1_fastqc.html",
            report1="analysis/3.trimmed/{file}_R1_001.fastq.gz_trimming_report.txt",            
            #fastqc2_pre="analysis/2.fastqc/{file}_R2_001_fastqc.html",
            trimmed2="analysis/3.trimmed/{file}_R2_001_val_2.fq.gz",
            fastqc2_post="analysis/2.fastqc/{file}_R2_001_val_2_fastqc.html",
            report2="analysis/3.trimmed/{file}_R2_001.fastq.gz_trimming_report.txt"

        shell:
            """
            test1=$(echo {params.ad1})
            echo "applying adapter sequence: ${{test1}}"
            if [[ ${{test1}} == "default" ]]; then adapter1=" " ; else adapter1=$(echo "-a ${{test1}}"); fi
            test2=$(echo {params.ad2})
            echo "applying reverse adapter sequence: ${{test2}}"
            if [[ ${{test2}} == "default" ]]; then adapter2=" " ; else adapter2=$(echo "-a2 ${{test2}}"); fi
            #fastqc {input.r1} -o analysis/2.fastqc/;
            #fastqc {input.r2} -o analysis/2.fastqc/;
            trim_galore \
            --paired \
            -o analysis/3.trimmed/ \
            -q {params.qual} \
            --fastqc \
            --fastqc_args '--outdir analysis/2.fastqc/' \
            --length {params.leng}  \
            --retain_unpaired $adapter1 $adapter2 \
            {input.r1} {input.r2}
            """

