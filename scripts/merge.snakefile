
rule merge:
    input:
        lambda wildcards: expand('analysis/8.Filtered/{file}.Mkdup.Filtered.bam', file = df.index[ df.Sample == wildcards.sample])
    output:
        "analysis/9.merge/{sample}.bam"
    shell:
        """
        echo -e 'input is {input} \n output is {output}'
        samtools merge -f {output} {input}
        """

rule MkDup_sam:
    input:
        bam="analysis/9.merge/{sample}.bam"
    output:
        bam="analysis/10.MkDup_per_sample/{sample}.Mkdup.bam",
        index="analysis/10.MkDup_per_sample/{sample}.Mkdup.bam.bai"
    shell:
        """
        export _JAVA_OPTIONS='-Xmx120G'
        picard MarkDuplicates INPUT={input.bam} OUTPUT={output.bam} METRICS_FILE=/dev/null REMOVE_DUPLICATES=false AS=true VALIDATION_STRINGENCY=SILENT TMP_DIR=tmp/ TAGGING_POLICY=All
        samtools index {output.bam}
        """
