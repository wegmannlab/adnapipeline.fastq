rule inbetween:
    input:
        trimmed=expand("analysis/trimmed/{{sample}}_{{lane}}_{{read}}_{no}_val_1.fq.gz", sample=SAMPLES, lane=FQ_LANE, read=FQ_READ, no=FQ_NO)
    output:
        inbet="analysis/trimmed/{sample}_{lane}_{read}.trimmed.fastq.gz"
    shell:
        """
        ls {input.trimmed} > {output.inbet}
        """


