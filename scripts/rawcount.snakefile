if (SEQUENCE == "single"):
    rule rawcount:
        input:
            r1=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz")
        output:
            out="analysis/rawcount/{file}_R1_001.raw.readcount"
        conda:
            "environment2.yaml"
        shell:
            "zcat {input.r1} | echo $(( $(wc -l)/4)) > {output.out}"

    rule fastqc:
        input:
            r1=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz")
        output:
            fastqc1="analysis/2.fastqc/{file}_R1_001_fastqc.html"
        conda:
            "environment2.yaml"
        shell:
            "fastqc {input.r1} -o analysis/2.fastqc"
        
else:
    rule rawcount:
        input:
            r1=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz"),
            r2=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R2_001.fastq.gz")
        output:
            out1="analysis/1.rawcount/{file}_R1_001.raw.readcount",
            out2="analysis/1.rawcount/{file}_R2_001.raw.readcount"            
        conda:
            "environment2.yaml"
        shell:
            """
            zcat {input.r1} | echo $(( $(wc -l)/4)) > {output.out1}
            zcat {input.r2} | echo $(( $(wc -l)/4)) > {output.out2}
            """
            
    rule fastqc:
        input:
            r1=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz"),
            r2=lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R2_001.fastq.gz")
        output:
            fastqc1_pre="analysis/2.fastqc/{file}_R1_001_fastqc.html",
            fastqc2_pre="analysis/2.fastqc/{file}_R2_001_fastqc.html"
        conda:
            "environment2.yaml"
        shell:
            """
            fastqc {input.r1} -o analysis/2.fastqc/;
            fastqc {input.r2} -o analysis/2.fastqc/
            """