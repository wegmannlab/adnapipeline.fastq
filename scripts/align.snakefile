if (ADAPTERS == "T"):
    rule align:
        input:
            trimmed()
        output:
            "analysis/4.alignments/{file}.bam"
        threads: 16
        conda:
            "environment2.yaml"
        shell:
            """
            bwa mem -t 8 -M {config[ref]} {input} | samtools view -bSh -q {config[mappingqual]} - > {output}
            """
if (ADAPTERS == "F"):
    if (SEQUENCE == "single"):
        rule align:
            input:
                lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz")
            output:
                "analysis/4.alignments/{file}.bam"
            threads: 16
            conda:
                "environment2.yaml"
            shell:
                """
                bwa mem -t 8 -M {config[ref]} {input} | samtools view -bSh -q {config[mappingqual]} - > {output}
                """    
    else:
        rule align:
            input:
                lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R1_001.fastq.gz"), 
                lambda wildcards: str(Path(FQ_PATH[wildcards.file]) / f"{wildcards.file}_R2_001.fastq.gz")
            output:
                "analysis/4.alignments/{file}.bam"
            threads: 16
            conda:
                "environment2.yaml"
            shell:
                """
                bwa mem -t 8 -M {config[ref]} {input} | samtools view -bSh -q {config[mappingqual]} - > {output}
                """    


rule sort_index:
    input:
        "analysis/4.alignments/{file}.bam"
    output:
        sort="analysis/5.sort/{file}.bam",
        index="analysis/5.sort/{file}.bam.bai"
    params:
        prefix="analysis/5.sort/{file}"
    shell:
        """
        rm -f {params.prefix}.bam.tmp.*.bam 
        samtools sort {input} -o {output.sort}
        samtools index {output.sort}
        """

rule newhead:
    input:
        "analysis/5.sort/{file}.bam"
    params:
        ID="{file}",
        LB=lambda wildcards: LIB[wildcards.file],
        SM=lambda wildcards: SAMPLE[wildcards.file]
    output:
        "analysis/6.newhead/{file}.bam"
    shell:
        """
        set +e
        echo -e "RGSM = {params.SM}"
        FASTQHEAD=$(samtools view {input} | head -1 | sed 's/" "/-/g' | cut -f 1 | cut -d ":" -f 1,2,3,4);
        picard AddOrReplaceReadGroups \
        I={input} \
        O={output} \
        ID={params.ID} \
        LB={params.LB} \
        PL=Illumina \
        PU=${{FASTQHEAD}} \
        SM={params.SM} \
        CN={config[CN]}
        exitcode=$?
        if [ $exitcode -eq 1 ]; then exit 1; else exit 0; fi
        """

rule MkDup_File:
    input:
        "analysis/6.newhead/{file}.bam"
    output:
        bam="analysis/7.MkDup_per_lib/{file}.Mkdup.bam",
        index="analysis/7.MkDup_per_lib/{file}.Mkdup.bam.bai"
    shell:
        """
        export _JAVA_OPTIONS='-Xmx120G'
        picard MarkDuplicates INPUT={input} OUTPUT={output.bam} METRICS_FILE=/dev/null REMOVE_DUPLICATES=false AS=true VALIDATION_STRINGENCY=SILENT TMP_DIR=tmp/ TAGGING_POLICY=All
        samtools index {output.bam}
        """

rule filtering:
    input:
        "analysis/7.MkDup_per_lib/{file}.Mkdup.bam"
    params:
        filter_mapped=filterMap()
    output:
        primary="analysis/8.Filtered/{file}.Mkdup.primary.bam",
        mapped="analysis/8.Filtered/{file}.Mkdup.Filtered.bam",
        index="analysis/8.Filtered/{file}.Mkdup.Filtered.bam.bai"
    shell:
        """
        samtools view -bh -F 256 {input} > {output.primary}
        samtools view -bh {params.filter_mapped} {output.primary} > {output.mapped}
        samtools index {output.mapped}
        """    


